#!usr/bin/python3.4
#
#     index_fixer.py
#
# In the exegesis of Sahih al-Bukhari (id 33), there is an error in the index of titles.
#
#  * The index index_33.json must be the same as index_24.py
#
#  * And the metadata in the file names must be adjusted in this way:
#
#      ** Files 33-77-128:180, must be changed to 33-78-0:52,
#      
#         e.g. 33-77-128 is turned into 33-78-0,
#              33-77-129 is tuened into 33-78-1, and so on
#
#      ** Files from bookid 33 with chapter ids >= 78, must add 1 to the id, so eg. 78 would be 79.
#
# usage:
#   $ python index_fixer.py --inf ../../data/all/merged --outf ../../data/all/fixed \
#                           --in_index ../../data/indexes/processed --out_index ../../data/indexes/fixed
#
#################################################################################################################

import os
import sys
import re
import json
import shutil
from argparse import ArgumentParser

def cleaner(text):
    """ Remove noisy characters from text.

    Args:
        text (str): Text to modify.

    Returns:
        str: Modified text.

    """
    text = text.replace('\t',' ')

    text = re.sub(r' +', ' ', text)

    # remove non-break space
    text = text.replace(chr(int('0xA0', 16)), '')

    # keep '«' (U+ab) and '»' (U+bb) untouched, and convert other quotation marks to '"'
    text.translate({ord(c) : None for c in '‘’‚‛“”„‟'})

    text = text.replace(chr(int('0x640', 16)), '')

    return text


def getindex(fpath_fname_list, myname):
    """ Get item from fileobj_name_li that match myname.

    Args:
        fpath_fname_list (list): sequence of pairs of file path and file name without extension.
        myname (str): name to search for.

    Return:
        (fpath, str): matched item with modifies name
            
    """
    for fpath, fname in fpath_fname_list:
        if fname == myname:
            return fpath, 'index_33'
    return None

def fix_name(fname):
    """ Change file names with index 33-77-128:180 to 33-78-0:52.

        e.g. "hadith.al-islam-1000-1000_33-77-128" is turned into "hadith.al-islam-1000-1000_33-78-0" and
             "hadith.al-islam-1000-1000_33-77-129" is turned into "hadith.al-islam-1000-1000_33-78-1"

    Args:
        fname (str): Original file name.

    Returns:
        str: Modified filename, with the extension .json added to the end.

    """
    pref, index = fname.split('_', 1)

    try:
        bookid, chapterid, *rest = index.split('-', 2)
    except ValueError:
        print('fname: "%s", index: "%s"' % (fname, index), file=sys.stderr)
        sys.exit(1)

    chapterid = int(chapterid)

    if bookid == '33':

        if chapterid == 77:

            subchapterid = int(rest[0])

            if subchapterid >= 128:

                index = '33-78-%d' % (subchapterid-128)

        elif chapterid > 77:

            index = '33-%s' % '-'.join((str(chapterid+1), *rest))

    return '%s_%s.json' % (pref, index)


if __name__ == '__main__':


    parser = ArgumentParser(description='fix errors in index and filenames for id 33')
    parser.add_argument('--inf', metavar='DATA_INPATH', action='store', help='input path  of data files')
    parser.add_argument('--outf', metavar='DATA_OUTPATH', action='store', help='output path of data files')
    parser.add_argument('--in_index', metavar='INDEX_INPATH', action='store', help='input path of index files')
    parser.add_argument('--out_index', metavar='INDEX_INPATH', action='store', help='output path of index files')
    
    args = parser.parse_args()

    #
    # process index files
    #

    # for each file in indexes get eg. (fpath, "index_30", ".json")
    index_infiles = ((f.path, *os.path.splitext(f.name)) for f in os.scandir(args.in_index) if f.is_file())

    # get list of all filenames exclusing the erroneous index_33
    index_infiles = [(fpath, fname) for fpath, fname, fext in index_infiles if fext=='.json' and fname!='index_33']

    # duplicate index_24 as if it were index_33
    index_infiles.append(getindex(index_infiles, 'index_24'))

    for src, fn in index_infiles:

        dst = os.path.join(args.out_index, fn + '.json')

        shutil.copyfile(src, dst)

    #
    # process data files
    #

    data_infiles = ((f.path, *os.path.splitext(f.name)) for f in os.scandir(args.inf) if f.is_file())

    data_infiles = ((fpath, os.path.join(args.outf, fix_name(fname)))  for fpath, fname, fext in data_infiles if fext=='.json')

    for src, dst in data_infiles:

        with open(src) as fp, \
             open(dst, 'w') as outfp:

            print('**', src, file=sys.stderr) 

            indata = json.load(fp)

            if len(indata.keys()) == 1:
                if 'original' in indata:
                    outdata = {'original' : cleaner(indata['original'])}
                else:
                    outdata = {'commentary' : cleaner(indata['commentary'])}

            elif len(indata.keys()) == 2:
                outdata = {'original' : cleaner(indata['original']), 'commentary' : cleaner(indata['commentary'])}
                
            else:
                print('Fatal error in structure of file "%s".' % src, file=sys.stderr)
                sys.exit(1)

            json.dump(outdata, outfp, ensure_ascii=False)

            #shutil.copyfile(src, dst)

