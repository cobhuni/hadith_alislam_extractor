#!/usr/bin/python3.4
#
#     selector.py
#
# It takes all json files from input directory matching the list
# in the reference file, and copies them to output directory.
#
# usage:
#   $ python selector.py references.json ../../data/all/fixed ../../data/prepared
#
##################################################################################

import os
import sys
import re
import json
import argparse


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='extracts texts from html files and dump into json files')
    parser.add_argument('reference_file', type=argparse.FileType('r'), help='file with list of file indexes to search')
    parser.add_argument('input_dir', action='store', help='input directory')
    parser.add_argument('output_dir', action='store', help='output directory')
    
    args = parser.parse_args()

    # load references file
    refs = json.load(args.reference_file)['exegesis']

    # get list of all input files to process
    fobjs = ((f, os.path.splitext(f.name)) for f in os.scandir(args.input_dir) if f.is_file())

    # extract index from file name
    fobjs_index = ((f, name_ext[0].split('_', 1)[1]) for f, name_ext in fobjs if name_ext[1]=='.json')

    # exclude indexes that are not in reference list. use regex to match also for sections
    target_files = ((f.name, f.path) for f, index in fobjs_index if any(re.match(r'^%s(\-\d+)?$' % r, index) for r in refs)) 

    # save files in output directory
    for fname, fpath in target_files:

        print('**', fname, file=sys.stderr)

        with open(fpath) as fp, \
             open(os.path.join(args.output_dir, fname), 'w') as outfp:
            
            json.dump(json.load(fp), outfp, ensure_ascii=False)
