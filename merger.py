#!usr/bin/python3.4
#
#     merger.py
#
# Merge all input files belonging to the same inner text subdivision, i.e., a chapter,
# it if has no subchapters, a subchapter, if it has no sections, or a section.
# 
#     infile name:  hadith.al-islam-BOOKID-PID-CHAPTERID[-SUBCHAPTERID-SECTIONID].json
#     outfile name: hadith.al-islam-PIDSTART-PIDEND_BOOKID-CHAPTERID[-SUBCHAPTERID-SECTIONID].json
#
# How to get list of all punctuation marks:
#   $ find . -name "*.json" -exec jq -r '(select(.original != null) | .original), (select(.commentary != null) | .commentary)' {} + |
#     grep -o . | sort | uniq | python -c "import sys; import unicodedata
#     for c in filter(None, (l.strip() for l in sys.stdin)): print(c, hex(ord(c)), unicodedata.name(c))"
#
# usage:
#   $ python merger.py ../../data/all/guessed/ ../../data/all/merged
#
###################################################################################################################################


import os
import sys
import ujson as json
from argparse import ArgumentParser
from itertools import groupby


PUNCT_AFTER = {
    ',', # 0x2c COMMA
    ';', # 0x3b SEMICOLON
    ':', # 0x3a COLON
    '!', # 0x21 EXCLAMATION MARK
    '?', # 0x3f QUESTION MARK
    '.', # 0x2e FULL STOP
    '،', # 0x60c ARABIC COMMA
    '؛', # 0x61b ARABIC SEMICOLON
    '؟'  # 0x61f ARABIC QUESTION MARK
    '>', # 0x3e GREATER-THAN SIGN
    ')', # 0x29 RIGHT PARENTHESIS
    '}', # 0x7d RIGHT CURLY BRACKET
    ']'  # 0x5d RIGHT SQUARE BRACKET
}

PUNCT_BEFORE = {
    '(', # 0x28 LEFT PARENTHESIS
    '[', # 0x5b LEFT SQUARE BRACKET
    '{'  # 0x7b LEFT CURLY BRACKET
}

def normalise_punct(doc):
    """ Adapt text to standard orthpgraphy so that an input like "foo , bar"
        is converted into "foo, bar".

    Args:
        doc (dict): object to modify. Texts are stored in values.
    
    """
    # process punctuation that comes after
    for texttype in doc:

        splited = ((k,''.join(txt).strip()) for k,txt in groupby(doc[texttype], key=lambda char: char in PUNCT_AFTER))
        doc[texttype] = ''.join(((txt if not key else ''.join((txt,' '))) for key,txt in splited))

    # process punctuation that comes before
    for texttype in doc:

        splited = ((k,''.join(txt).strip()) for k,txt in groupby(doc[texttype], key=lambda char: char in PUNCT_BEFORE))
        doc[texttype] = ''.join(((''.join((txt,' ')) if not key else txt) for key,txt in splited))


if __name__ == '__main__':

    parser = ArgumentParser(description='extracts texts from html files and dump into json files')
    parser.add_argument('input_dir', action='store', help='input directory with json files')
    parser.add_argument('output_dir', action='store', help='output directory where merged files will be saved')
    args = parser.parse_args()

    # get for each file a tuple with the info (inpath, fname, bookid, pid, chapterid, ...)
    files_info = ((f.path, f.name, *tuple(map(int, f.name.rsplit('.')[-2].split('-')[2:])))
                   for f in os.scandir(args.input_dir) if f.is_file() and os.path.splitext(f.name)[1]=='.json')

    # sort files by BOOKID and PID
    files_info_sorted = sorted(files_info, key=lambda x: tuple(map(int, x[2:4])))

    index_prev = []
    pid_prev = -1
    pid_start = -1
    obj_aux = {}
    
    for fileinfo in files_info_sorted:
        inpath, fname, bookid, pid_current, chapterid, *rest = fileinfo

        print('\n**', fname, file=sys.stderr)  #DEBUG

        index_current = [bookid,chapterid]
        
        # process subchapter
        if rest:
        
            index_current.append(rest.pop(0))

            # process section
            if rest:

                index_current.append(rest.pop(0))

            if rest:
                print('Error processing name info of file "%s".' % inpath, file=sys.stderr)
                sys.exit(1)

        with open(inpath) as fp:
            obj_current = json.load(fp)

        # new object
        if index_current != index_prev:

            if obj_aux:

                outfname = 'hadith.al-islam-%d-%d_%s.json' % (pid_start, pid_prev, '-'.join(map(str, index_prev)))

                print('>>', outfname, file=sys.stderr)  #DEBUG

                # save prev obj
                with open(os.path.join(args.output_dir, outfname), 'w') as outfp:
                    json.dump(obj_aux, outfp, ensure_ascii=False)

            # prepare new object to store info
            obj_aux = obj_current
            pid_start = pid_current
            index_prev = index_current

        # continue with previous object
        else:

            if 'original' in obj_aux and 'original' in obj_current:
                obj_aux['original'] += ' %s' % obj_current['original'] #FIXME

            if 'commentary' in obj_current:
                obj_aux['commentary'] = obj_aux.get('commentary', ' %s' % obj_current['commentary']) + ' %s' % obj_current['commentary'] #FIXME

        pid_prev = pid_current

    #TODO normalise punctuation
    normalise_punct(obj_aux)


    # save last file
    outfname = 'hadith.al-islam-%d-%d_%s.json' % (pid_start, pid_prev, '-'.join(map(str, index_prev)))

    print('>>', outfname, file=sys.stderr)  #DEBUG

    with open(os.path.join(args.output_dir, outfname), 'w') as outfp:
        json.dump(obj_aux, outfp, ensure_ascii=False)

