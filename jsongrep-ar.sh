#!/bin/bash
#
#     jsongrep-ar.sh
#
# search arabic text regardless vocalization
# in json struct and return indexes.
#
# exmaple:
#   $ cat ~/COBHUNI/.../index_25.json | bash jsongrep-ar.sh
#
# example of how to recheck index on json:
#   $ cat ~/COBHUNI/.../index_25.json | jq -r '.[45] | .name, .subchapters[1].name' | fribidi
#
#############################################################################################

jsonpipe |           # convert json struct into lines

ascii2uni -a U -q |  # show arabic utf8

grep '"' |           # grep only lines with text

python -c "          # remove vocalization
import sys
import re
from kabkaj.util import normalise
for line in filter(None, map(str.strip, sys.stdin)):
    index, name = line.split(None, 1)
    name = normalise.remove_diacritics(re.sub(r' +', ' ', name))
    if 'باب غسل ' in name:
        print(index, name, sep='    ')
"
