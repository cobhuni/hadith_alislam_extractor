#!/usr/bin/env python
#
#     guesser.py
#
# Get exegesis documents (bookid 33-39) with only one block of text (it was marked as
# original by the previous process), and verify it is indeed original by finding it
# in the original documents (bookid 24-32)
# 
# The format of the file name is as follows:
#
#     infile name:  hadith.al-islam-BOOKID-PID-CHAPTERID[-SUBCHAPTERID-SECTIONID].html
#     outfile name: hadith.al-islam-BOOKID-PID-CHAPTERID[-SUBCHAPTERID-SECTIONID].json
#
#    json format of files of books with id in range 24-32:
#
#        { "original" : text }
#
#    json format of files of books with id in range 33-39:
#
#        { "original" : text,
#          "commentary" : text }
#
# usage:
#   $ python guesser.py ../../data/all/filtered ../../data/all/guessed
#
#######################################################################################

import os
import sys
import ujson as json
from argparse import ArgumentParser
from itertools import tee
from functools import partial
import shutil


def processfile(fobj, originalpaths, outfpath):
    """ Check if there is only one block of text in fobj. If so,
        check if text is original or exegesis by comparing it with
        all original texts. If not, end function.

    Args:
        cnt (int): keep track of number of files processed.
        fobj (posix.DirEntry): file object to parse.
        originalpaths (list): paths of original paths to do the comparisons against their text
            when a one-block-of-text document if found.
        outfpath (str): output path.

    """
    with open(fobj.path) as fp:
        jobj = json.load(fp)

        ORIGINAL = False

        # document containig only one block of text
        if 'commentary' not in jobj:

            print('\n[DEBUG] one-text-block found in file', fobj.name, file=sys.stderr)

            guesstext = jobj['original']

            for oripath in originalpaths:

                with open(oripath) as orifp:
                    oritext = json.load(orifp)['original']

                    if guesstext == oritext:
                        print('    original,', fobj.name, file=sys.stderr) #DEBUG
                        ORIGINAL = True
                        break

            if ORIGINAL == False:
                print('    commentary', file=sys.stderr) #DEBUG
                jobj['commentary'] = jobj.pop('original')


    with open(outfpath, 'w') as outfp:
        json.dump(jobj, outfp, ensure_ascii=False)



if __name__ == '__main__':

    parser = ArgumentParser(description='verify isolated blocks of text in exegesis are indeed original, or modify label')
    parser.add_argument('input_dir', action='store', help='input directory with json files')
    parser.add_argument('output_dir', action='store', help='output directory with corrected json files')
    args = parser.parse_args()

    
    files = ((fobj, *os.path.splitext(fobj.name)) for fobj in os.scandir(args.input_dir) if fobj.is_file())

    fobjs = ((int(fname.split('-')[2]), fobj) for fobj, fname, fext in files if fext=='.json')

    orifiles, exefiles = (lambda A,B: ((fo for id_,fo in A if id_<=32), (fo for id_,fo in B if id_>=33)))(*tee(fobjs))  #COMMENT to customise
    #orifiles, exefiles = (lambda A,B: ((fo for id_,fo in A if id_<=32), (fo for id_,fo in B if id_>=38)))(*tee(fobjs))  #FIXME customise and comment

    orifiles, orifiles_ = tee(orifiles)

    oripaths = [f.path for f in orifiles_]

    for f in exefiles: processfile(f, oripaths, os.path.join(args.output_dir, f.name))

    for f in orifiles: shutil.copyfile(f.path, os.path.join(args.output_dir, f.name)) #COMMENT to customise
