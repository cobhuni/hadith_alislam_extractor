#!usr/bin/python3.4
#
#     indexer.py
#
# Extracts chapter, subchapter and section names from all html files, observing nesting sequence,
# and creates an index with all title nesting information of all collections.
# 
# Copy input files into output folder adding the index info into the filename.
# 
# Only copies files that follow the sequence of next pages indicated within a html tag.
#
#   Structure of names of files:
#
#     infile name:  hadith.al-islam-BOOKID-PID-TIMESTAMP.html
#     outfile name: hadith.al-islam-BOOKID-PID-CHAPTERID[-SUBCHAPTERID-SECTIONID].html
#    
#     NOTE: PID is a counter that mantains the text sequence within each book, starting always from 1
#           It is used in the web page. We keep it to be able to map our texts with its web page
#
#   Structure of index.json:
#
#       [ { "name": "kitab ..." ,
#           "subchapters": [ { "name": "bab ..." ,
#                              "sections": [ { "name": "subbab ..." } ,
#                                            { "name": "subbab ..." } ,
#                                            ...
#                                          ],
#                            } ,
#                            ...
#                          ] , 
#         } ,
#         { "name": "kitab ..." ,
#           "subchapters": [ {
#                             "name": "bab ..." ,
#                             "sections": [] ,
#                            } ,
#                          ...
#                          ] ,
#         },
#         { "name": "kitab ..." ,
#           "subchapters": [] ,
#         },
#         ...
#       ]
#
# usage:
#   $ for i in {24..39} ; do
#   $     python indexer.py --id $i ../../data/all/ori/ ../../data/all/reduced ../../data/indexes/all_titles.txt ../../data/indexes/processed/index_$i.json
#   $ done
#
###########################################################################################################################################################

import os
import sys
import re
import json
from bs4 import BeautifulSoup
from argparse import ArgumentParser, FileType
import shutil
from pprint import pprint
from config import PID_max, ID_RANGE, ID_MAX, ID_MIN

from abjad_utilities.abjad_utilities import AbjadUtilities

# beginning of tag that contains the titles
TITLES_TAG = 'ctl00_pageWebPartManager_wp79505043_wp247801327_ucViewPath_RepTocPath'

# to follow pages along html files
PAGE_TAG = 'ctl00_pageWebPartManager_wp1020548165_wp1879933950_ucViewPageWithShroo7_ucContentPagingLast_tdNext'
PAGE_NEXT = 'ctl00_pageWebPartManager_wp1020548165_wp1879933950_ucViewPageWithShroo7_ucContentPagingLast_lnkNext'

###############################################################################

#
# functions
#

def process_file(fname):
    """ Gets book, chapter and subchapter names from fname file

    Args:
        fname (str): Input filename to parse and filter.

    Returns:
        list: list of titles extracted from the file.

    """
    with open(fname) as inf:
        soup = BeautifulSoup(inf.read(),'lxml')

    # get all titles in file
    titles_elems = soup.findAll(lambda tag: tag.name=='a' and 'id' in tag.attrs and tag.attrs['id'].startswith(TITLES_TAG))
    
    # clean strings
    titles = list(re.sub('\s+',' ', elem.text).strip() for elem in titles_elems)

    if len(titles)==1:
        print('Error in file "%s": only name of collection found' % (fname), file=sys.stderr)  #DEBUG
        sys.exit(1)

    print('\n**', fname.ljust(60), '|'.join(titles).rjust(60), file=sys.stderr)  #DEBUG

    # exclude name of collection
    return titles[1:]

def select_vocalised(s, li):
    """ Select vocalised version of string from li.

    Args:
        s (str): string to check and replace by vocalised version.
        li (list): list of all possible strings.

    Returns:
        str: string from li that is the vocalised version of s.
             None if no equivalent is found.

    """
    for var in li:
        if AbjadUtilities.remove_diacritics(var) == s:
            return var

    print('Error: string "%s" is not included in the complete list' % s, file=sys.stderr) #DEBUG
    sys.exit(1)

def getNextPage(fn):
    """ read fn file, which is a html file. and extract the information of next page.

    Args:
        fn (str): file to read and parse.

    Return:
        (str): next page or None is no next page is found.

    """
    with open(fn) as inf:
        soup = BeautifulSoup(inf.read(),'lxml')

    # search page info
    page_found = soup.find(id=PAGE_TAG)

    if not page_found:
        print('Error if file "%s": No info about page.' % fn, file=sys.stderr) #DEBUG
        sys.exit(1)  #DEBUG

    page_next = page_found.find(id=PAGE_NEXT)

    if page_next:
        nextp = page_next['href'].rsplit('=',1)[-1]
        #print('  nextp =" %s' % nextp, file=sys.stderr) #DEBUG
        return nextp

    # no next page in file. end of sequence
    return None

def getSequence(files):
    """ get sequence of files acording to next page information read from each file,
        starting from first file.

    Args:
        files (list):  list with all filenames.

    Yields:
        str: filenames that follow the sequence of next pages.

    """ 
    if not files:
        print('Error: no files to read', file=sys.stderr)
        sys.exit(1)

    currentf = files.pop(0)
    yield currentf

    nextp = getNextPage(currentf)
    if not nextp: return

    while files:

        currentf = files.pop(0)
        currentp = os.path.basename(currentf).split('-')[3]

        if currentp == nextp:
            yield currentf

            nextp = getNextPage(currentf)
            if not nextp: return
            
    print('Error: file corresponding to next page not found', file=sys.stderr)
    sys.exit(1)


###############################################################################


if __name__ == '__main__':

    parser = ArgumentParser(description='extracts relevant info from html files and dumps into json files')
    parser.add_argument('--id', action='store', type=int, choices=ID_RANGE, metavar='[%d-%d]' % (ID_MIN,ID_MAX),
                                           required=True, help='id of compilation to create index')
    parser.add_argument('input_dir', action='store', help='input directory with html files')
    parser.add_argument('out_dir', action='store', help='output directory for process html files')
    parser.add_argument('titles_file', type=FileType('r'), help='text file containing list of all titles')
    parser.add_argument('outfile', type=FileType('w'), help='json file to save index')
    args = parser.parse_args()

    # read and store titles
    alltitles = set(filter(None, (re.sub('\s+',' ',l.strip()) for l in args.titles_file)))

    # get filenames from input directory
    fpaths = (f.path for f in os.scandir(args.input_dir) if f.is_file() and os.path.splitext(f.name)[1]=='.html')

    # list all html files of selected id
    files_myindex = filter((lambda f: f.endswith('.html') and 'INDEX' not in f and int(f.split('-')[2])==args.id), fpaths)

    # sort filenames of selected PID
    files_myindex_sorted = sorted(files_myindex, key=lambda f: int(f.split('-')[3]))

    # get sequence of files to process following next pages
    files_myseq = getSequence(files_myindex_sorted)

    # struct to be saved
    index = []

    # to check previous title read
    prev_title = tuple()

    for fname in files_myseq:

        titles_voc = tuple(select_vocalised(t, alltitles) for t in process_file(fname))

        # if titles are diferent to previous ones read, then add them into index
        if titles_voc != prev_title:

            chapter, *rest = titles_voc
            
            if not index or index[-1]['name'] != chapter:
                index.append({'name' : chapter, 'subchapters' : []})

                # index for filename
                index_fname = [len(index)-1]

            if rest:
                subchapter, *rest = rest

                if not index[-1]['subchapters'] or index[-1]['subchapters'][-1]['name'] != subchapter:

                    index[-1]['subchapters'].append({'name' : subchapter, 'sections' : []})

                    # update index for filename
                    index_fname = index_fname[:1] + [len(index[-1]['subchapters'])-1]

                if rest:
                
                    section = rest[0]

                    if not index[-1]['subchapters'][-1]['sections'] or index[-1]['subchapters'][-1]['sections'][-1]['name'] != section:

                        index[-1]['subchapters'][-1]['sections'].append({'name' : section})

                        # update index for filename
                        index_fname = index_fname[:2] + [len(index[-1]['subchapters'][-1]['sections'])-1]

            prev_title = titles_voc

        # filename including index for saving in out folder
        newfname = '%s-%s.html' % (os.path.basename(fname).rsplit('-',1)[0], '-'.join(map(str,index_fname)))

        print('>>', newfname, file=sys.stderr)  #DEBUG

        # copy file to outfolder changing only the name
        shutil.copy(fname, os.path.join(args.out_dir, newfname))


    # save index with all info of book, chapter and subchapters tree
    json.dump(index, args.outfile, ensure_ascii=False)

