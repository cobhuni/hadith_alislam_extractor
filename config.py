#!usr/bin/python3.4
#
#     config.py
#
#
# configuration data
# ids for scrapping hadith.al-islam.com
#
###########################################

#                compilation name  :  compilation id
COMPILATIONS = {
                 # ****** original ******
                 "صحيح البخاري" : 24 ,  # Sahih Bukhari
                 "صحيح مسلم"    : 25 ,  # Sahih Muslim
                 "سنن الترمذي"  : 26 ,  # Sunan al-Tirmidhi
                 "سنن النسائي"  : 27 ,  # Sunan al-Nasa'i (also known as السنن الصغرى Al-Sunan al-Sughra)
                 "سنن أبي داود" : 28 ,  # Sunan Abu Dawud
                 "سنن ابن ماجه" : 29 ,  # Sunan ibn Majah
                 "مسند أحمد"    : 30 ,  # Musnad Ahmad (ibn Hanbal)
                 "موطأ مالك"    : 31 ,  # Muwatta Malik
                 "سنن الدارمي"  : 32 ,  # Sunan al-Darimi

                 # **** commentaries ****
                 "فتح الباري شرح صحيح البخاري"           : 33 ,  # Fath al-Bari sharh Sahih Bukhari
                 "صحيح مسلم بشرح النووي"                 : 34 ,  # Sahih Muslim bi-sharh al-Nawawi
                 "تحفة الأحوذي شرح سنن الترمذي"          : 37 ,  # Tuhfat al-Ahwadhi sharh Sunan al-Tirmidhi
                 "سنن النسائي شرح السيوطي وحاشية السندي" : 38 ,  # Sunan al-Nasa'i sharh al-Suyuti wa-hashiyat al-Sindi
                 "عون المعبود شرح سنن أبي داود"          : 36 ,  # 'Awn al-Ma'bud sharh Sunan Abi Dawud
                 "سنن ابن ماجه بشرح السندي"              : 35 ,  # Sunan Abi Majah bi-sharh al-Sindi
                 "المنتقى شرح موطأ مالك"                 : 39 ,  # al-Muntaqa sharh Muwatta Malik
}


# total number of pages each book has in the webpage
#
#            compilation id  : last page of collection (PID variable)
PID_max = {   
              # original
              24 : 7236 , 
              25 : 5434 ,
              26 : 3977 ,
              27 : 5669 ,
              28 : 4592 ,
              29 : 4333 ,
              30 : 26365 ,
              31 : 2060 ,
              32 : 3368 ,

              # commentaries
              33 : 13851 , 
              34 : 8651 , 
              37 : 7799 , 
              38 : 6820 , 
              36 : 9147 , 
              35 : 8007 , 
              39 : 3918 , 
}

# calculate min, max and range of compilation ids
ID_MIN = min(PID_max.keys())
ID_MAX = max(PID_max.keys())
ID_RANGE = range(ID_MIN, ID_MAX+1)
