#!usr/bin/python3.4
#
#     crawler.py
#
# downloads hadith material from hadith.al-islam.com
# (url state from january 2016)
#
#     outfile name:  hadith.al-islam-BOOKID-PID-TIMESTAMP.html
#
# Dependencies:
#   config.py
#
# Usage:
#   $ for i in {24..39} ; do
#   $     python crawler.py --id $i ../../data/all/ori --debug
#   $ done
#
###############################################################

from sys import stderr,exit
from os.path import join as joinpath
from argparse import ArgumentParser
from bs4 import BeautifulSoup
from urllib import parse
from urllib.request import urlopen,Request
from urllib.error import HTTPError
from time import time,sleep
from config import PID_max, ID_RANGE, ID_MAX, ID_MIN
from random import uniform


parser = ArgumentParser(description='download hadith material from hadith.al-islam.com')
parser.add_argument('outdir', action='store', help='directory to store all html files')
parser.add_argument('--id', action='store', type=int, choices=ID_RANGE, metavar='[%d-%d]' % (ID_MIN,ID_MAX),
                                           required=True, help='id of compilation to download')
parser.add_argument('--debug', action='store_true', help='print each query data to stderr')
parser.add_argument('--only_index', action='store_true', help='download only index with books and chapter names')

args = parser.parse_args()

# ────────────────────────────────────────────────────
# constants and configuration
# ────────────────────────────────────────────────────

SLEEP_MIN = 0.986
SLEEP_MAX = 11.52

URL_texts = 'http://hadith.al-islam.com/Page.aspx'    # url for retreiving text files
URL_index = 'http://hadith.al-islam.com/Loader.aspx'  # url for retreiving index pages, i.e. list of book and chapter names

# variables to add to the query, for text pages
VARS_texts = { 'pageid'  : 192,   # fixed value, for retrieving text files
               'bookID'  : 0,     # 24-32, depends on config.COMPILATIONS
               'PID'     : 1,     # 1-n, loop until config.PID_max
}

# variables to add to the query, for index pages
VARS_index = { 'pageid' : 194 ,   # fixed value, for retrieving index pages
               'bookID' : 0 ,
}

USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/44.0.2403.89 Chrome/44.0.2403.89 Safari/537.36'
HEADERS = {'User-Agent' : USER_AGENT}

# ────────────────────────────────────────────────────
# functions
# ────────────────────────────────────────────────────

def addData2Url(url, data):
    """Attach variables to url.

    Args:
        url (str): Input url.
        data (dict): Variables to attach to url.

    Return:
        str: Modified url.

    """
    datastr = '&'.join(['%s=%s' % (k,v) for k,v in  data.items()])
    return '%s?%s' % (url, datastr)

def requestURL(url, data):
    """Request URL and return html string.

    Args:
        url (str): Url to query.
        data (dict): Variables of the query as name:value.

    Return:
        bs4.BeautifulSoup: Resulting html from the query or None if an error occurred.

    """
    fullurl = addData2Url(url, data)
    req = Request(fullurl, headers=HEADERS)

    try:
        resp = urlopen(req)
    except HTTPError as e:
        print('Error opening url.', file=stderr)
        exit(1)

    return BeautifulSoup(resp.read(), 'lxml')

def saveRetrivedURL(soup, prefix_outf, outdir, info):
    """Saves html string into a file.

    Args:
        soup (bs4.BeautifulSoup): Result of query.
        prefix_outf (str): Prefix name of the output file.
        outdir (str): Path of output directory. 
        info (list): Sorted list of int variables to add to outname.

    """
    fname = '%s-%s-%d.html' % (prefix_outf, '-'.join(list(map(str,info))), int(time()))

    with open(joinpath(outdir, fname), 'w') as outf:
        print(soup.prettify(), file=outf)

    return


# ────────────────────────────────────────────────────
# main
# ────────────────────────────────────────────────────

# set id of book collection to be queried
VARS_index['bookID'] = VARS_texts['bookID'] = args.id


#
# retrieving index page
#

respData = requestURL(URL_index, VARS_index)

# save html for each query
saveRetrivedURL(respData, 'hadith.al-islam-INDEX', args.outdir, [VARS_index['bookID']])

if args.debug:
    print('** INDEX query   bookID=%s' % (args.id), file=stderr)

# stop downloading if only index page flag set on
if args.only_index: exit()


#
# retrieving texts
#

# request all the pages corresponding to the selected id id
#for pid in range(1, PID_max[args.id]+1):
for pid in range(142, 143):

    # update number of page of query
    VARS_texts['PID'] = pid

    # request, open and read url
    respData = requestURL(URL_texts, VARS_texts)

    if args.debug:
        print('query   bookID=%s PID=%s' % (VARS_texts['bookID'], VARS_texts['PID']), file=stderr)

    # save html for each query
    saveRetrivedURL(respData, 'hadith.al-islam', args.outdir, [VARS_texts['bookID'], VARS_texts['PID']])

    # wait random 6.7 avg time to do next query
    sleep(uniform(SLEEP_MIN, SLEEP_MAX))

