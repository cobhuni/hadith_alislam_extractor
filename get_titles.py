#!usr/bin/python3.4
#
#     get_titles.py
#
# It gets all titles from *.clipboard files. In case the same title appears
# more than one, selects the version with the most complete vocalization
#
# usage:
#   $ python get_titles.py ../../data/indexes/original/ ../../data/indexes/all_titles.txt
#
########################################################################################

import os
import sys
import re
from itertools import chain, groupby
from argparse import ArgumentParser, FileType

from abjad_utilities.abjad_utilities import AbjadUtilities


# word preceding some titles
MENU_MARK = 'إغلاق'

###########################################################################

#
# parse args
#

parser = ArgumentParser(description='Cleans and extracts titles from *.clipboard.'
                                    ' Exclude repetitions and select best orthographic variant of each group of similar variants.')
parser.add_argument('input_dir', help='input directory with .clipboard files')
parser.add_argument('outfile', nargs='?', type=FileType('w'), default=sys.stdout, help='filename to save parsed titles')
parser.add_argument('--debug', action='store_true', help='debug mode')
args = parser.parse_args()


###########################################################################

#
# functions
#

def getlines(fn):
    """ Read lines from file with name fn.

    Args:
        fn (string): Filename.

    Yield:
        string: A line read from file if not empty.

    """
    with open(fn) as inf:

        for line in filter(None, (l.strip() for l in inf)):

            if args.debug: print('\n(0) line       = "%s"' % line, file=sys.stderr)  #DEBUG

            # remove initial word not belonging to titles
            clean_line = re.sub(r'^%s' % MENU_MARK, '', line)

            if args.debug: print('(1) clean line = "%s"' % clean_line, file=sys.stderr)  #DEBUG

            # remove punctuation
            clean_line = ''.join(c for c in clean_line if c not in "[]():'")

            if args.debug: print('(2) clean line = "%s"' % clean_line, file=sys.stderr)  #DEBUG

            clean_line = clean_line.strip()

            if args.debug: print('(3) clean line = "%s"' % clean_line, file=sys.stderr)  #DEBUG
            
            yield clean_line

def getLongest(li):
    """ Return longest string from li. If more than one
    have longest length, returns all of them.

    Args:
        li (list): List of strings to filter

    Return:
       list: Group of longest strings.

    """
    lengths = list(map(len, li))
    
    maxlen = max(lengths)
    
    len_words = filter(lambda x: x[0]==maxlen, zip(lengths, li))
    
    return list(s[1] for s in len_words)

###########################################################################

#
# main
#

if __name__ == '__main__':

    # get all files to process
    fnames = (os.path.join(args.input_dir,f) for f in os.listdir(args.input_dir) if f.endswith('.clipboard'))

    # get all titles from files
    titles = chain(*(getlines(fn) for fn in fnames))

    # sort titles based on non-vocalized versions
    titles_sort = sorted(titles, key=lambda t: AbjadUtilities.remove_diacritics(t))

    # group titles based on identical non-vocalized versions and store uniq versions of exact identicals
    titles_grouped = list(set(g) for k,g in groupby(titles_sort, key=lambda t: AbjadUtilities.remove_diacritics(t)))

    # get longest string(s) from titles
    titles_longest = (getLongest(t) for t in titles_grouped)

    #NOTE there are 35 cases with different vocalised titles with same (max) length
    #     we select randomly the first one as the best orthographic version of the title
    #     and store it in the output

    for title in titles_longest:
        print(title[0], file=args.outfile)

