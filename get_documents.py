#!/usr/bin/env python
#
#     get_documents.py
#
# sort selection of filenames by index.
#
# format: "hadith.al-islam-PAGESTART-PAGEEND_BOOKID-CHAPTERID-[SUBCHAPTERID-SECTIONID].json"
#
# example of usage:
#   $ python get_documents.py ../../data/all/merged/ --bookid 24 --chapter 0,4
#
# command to check content of originals:
#   $ diff --suppress-common-lines -y <(for f in $(python get_documents.py ../../data/all/merged/ --bookid 24 --chapter 0,0) ; do cat $f | jq .original ; done | tr ' ' '\n' | trim | grep .) <(for f in $(python get_documents.py ../../data/all/merged/ --bookid 33 --chapter 0,0) ; do cat $f | jq .original ; done | tr ' ' '\n' | trim | grep .)
#
# to compare names of chapters:
#   $ diff --suppress-common-lines -y <(cat index_24.json | jsonpipe | ascii2uni -a U -q | grep -P "^/[0-9]+/name *.+" | sed -r 's/^\/[0-9]+\/name.*"(.+)"$/\1/') <(cat index_33.json | jsonpipe | ascii2uni -a U -q | grep -P "^/[0-9]+/name *.+" | sed -r 's/^\/[0-9]+\/name.*"(.+)"$/\1/')
# 
# to compare names of subchapters:
#   $ diff --suppress-common-lines -y <(cat index_24.json | jsonpipe | ascii2uni -a U -q | grep -P "^/[0-9]+/subchapters/[0-9]+/name *.+" | sed -r 's/^\/[0-9]+\/subchapters\/[0-9]+\/name.*"(.+)"$/\1/') <(cat index_33.json | jsonpipe | ascii2uni -a U -q | grep -P "^/[0-9]+/subchapters/[0-9]+/name *.+" | sed -r 's/^\/[0-9]+\/subchapters\/[0-9]+\/name.*"(.+)"$/\1/')
#
# to compare names of sections:
#   $ diff --suppress-common-lines -y <(cat index_24.json | jsonpipe | ascii2uni -a U -q | grep -P "^/[0-9]+/subchapters/[0-9]+/sections/[0-9]+/name *.+" | sed -r 's/^\/[0-9]+\/subchapters\/[0-9]+\/sections\/[0-9]+\/name.*"(.+)"$/\1/') <(cat index_33.json | jsonpipe | ascii2uni -a U -q | grep -P "^/[0-9]+/subchapters/[0-9]+/sections/[0-9]+/name *.+" | sed -r 's/^\/[0-9]+\/subchapters\/[0-9]+\/sections\/[0-9]+\/name.*"(.+)"$/\1/')
#
##################################################################################

import sys
import os
from argparse import ArgumentParser, ArgumentTypeError

def indexes(arg):
    try:
        ini,end = arg.split(',', 1)
        return tuple((int(ini), int(end)))

    except:
        raise ArgumentTypeError('Kitab initial and final indexes have to be formated as i,j')

if __name__ == '__main__':

    parser = ArgumentParser(description='sort selection of filenames by index')
    parser.add_argument('input_dir', action='store', help='input directory')
    parser.add_argument('--bookid', required=True, help='id of book to get sorted filenames')
    parser.add_argument('--chapter', required=True, type=indexes, help='initial and ending indexes i,j')
    parser.add_argument('--subchapter', required=False, type=indexes, help='initial and ending indexes i,j')

    args = parser.parse_args()

    def is_in_range(index, chapter_range, subchapter_range):
        chapter_index, *rest_indexes = index.split('-')
        chapter_index = int(chapter_index)
        if not subchapter_range:
            return (chapter_index >= chapter_range[0] and chapter_index <= chapter_range[1])    
        else:
            subchapter_index = None if not len(rest_indexes) else int(rest_indexes[0])
            return (chapter_index >= chapter_range[0] and chapter_index <= chapter_range[1]) and \
                   (subchapter_index >= subchapter_range[0] and subchapter_index <= subchapter_range[1])

    # get list of all input files to process
    fobjs = ((f, os.path.splitext(f.name)) for f in os.scandir(args.input_dir) if f.is_file())

    # extract id_book and rest_of_index from file name
    fobjs_index = ((f, *name_ext[0].split('_', 1)[1].split('-', 1)) for f, name_ext in fobjs if name_ext[1]=='.json')

    # get index 
    fobjs_selected = ((f, tuple(map(int, index.split('-')))) for f, bookid, index in fobjs_index
                         if bookid==args.bookid and is_in_range(index, args.chapter, args.subchapter))

    for fobj, index in sorted(fobjs_selected, key=lambda x: x[1]):
        print(fobj.path)

    
